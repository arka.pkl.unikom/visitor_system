<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }
 
  function index(){
    //Allowing akses to admin only
      if($this->session->userdata('access')==='admin'){
          $this->load->view('admin/index');
      }else{
          echo "Access Denied";
      }
 
  }
 
  function user(){
    //Allowing akses to staff only
    if($this->session->userdata('access')==='user'){
      $this->load->view('user/index');
    }else{
        echo "Access Denied";
    }
  }
 
 
}